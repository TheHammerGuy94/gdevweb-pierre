export let AssetDirectory = {
	"load": [
		"assets/images/Circle.png",
		"assets/images/Square.png",
		"assets/images/Triangle.png"
	],
	"audio": [
		"assets/audio/ColorWhere.mp3"
	]
};