import { Container,utils, Text,Graphics,Sprite} from "pixi.js";
var Config = require('Config');

class TitleScreen extends Container{
    constructor(){
    	super();
    	this.bgmInstance = createjs.Sound.play("assets/audio/ColorWhere.mp3",{
       		loop: -1,
       		volume: 1.f
       	});
       	this.bgmPlay = true;


    }

    function toggleplaystop(audio, isPlaying){
    	if(isPlaying)
    		audio.stop();
    	else
    		audio.play();


    }


}

export default TitleScreen;
function drawGrid(stage){
	var grd = new PIXI.Graphics();
	grd.lineStyle(2,0x808080);
	for(let i = 100; i< Config.BUILD.HEIGHT; i+= 100){
		//draw H lines
		
		grd.moveTo(0,i);
		grd.lineTo(Config.BUILD.WIDTH,i);
	}
	grd.lineStyle(1,0x808080);
	for(let i = 10; i< Config.BUILD.HEIGHT; i+= 10){
		//draw smol H lines
		if(i%100 != 0){
			grd.moveTo(0,i);
			grd.lineTo(Config.BUILD.WIDTH,i);
		}
	}

	grd.lineStyle(2,0x808080);
	for(let i = 100; i< Config.BUILD.WIDTH; i+= 100){
		//draw V lines
		grd.moveTo(i,0);
		grd.lineTo(i,Config.BUILD.HEIGHT);
	}
	grd.lineStyle(1,0x808080);
	for(let i = 10; i< Config.BUILD.WIDTH; i+= 10){
		//draw V lines
		if(i%100 != 0){
			grd.moveTo(i,0);
			grd.lineTo(i,Config.BUILD.HEIGHT);
		}
	}
	stage.addChild(grd);
}
