var width = 1280, height =720;
const app = new PIXI.Application({
	"autoStart" :true,
	"width" : width,
	"height" : height,
	"view" : document.getElementById("game_canvas"),
	"backgroundColor" : 0X000000,
	"transparent" : false,
	"antialias" : true,
	"preserveDrawingBuffer" :false,
	"resolution" : 1,
	"forceCanvas" :true,
	"clearBeforeRender" :true,
	"roundPixels" :true,
	"forceFXAA" :true,
	"legacy" :false,
	"powerPreference" : "",
	"sharedTicker" :false,
	"sharedLoader" :false
});
app.stage.ticker = PIXI.ticker.shared;

var moveY = 0;
var moveX = 0;
var speed = 500;
var movingObj = new PIXI.Graphics();
movingObj.beginFill(0xFFFFFF);
movingObj.drawCircle(0,0, 50);
app.stage.addChild(movingObj);

function move(dtime){
	movingObj.x += moveX*dtime*speed;
	movingObj.y -= moveY*dtime*speed;
}
function movementInput(e){

	switch(e.keyCode){
		case KeyCodes.left: moveX = -1;
			break;
		case KeyCodes.right: moveX = 1;
			break;
		case KeyCodes.down: moveY = -1;
			break;
		case KeyCodes.up: moveY = 1;
			break;

	}
}

function unMove(){
	moveX = moveY = 0;
}


function onKeyDown(e){
	movementInput(e);
}

function onKeyUp(e){
	unMove();
}

//window.addEventListener("keypress", onKeyPress.bind(this));
window.addEventListener("keydown", onKeyDown.bind(this));
window.addEventListener("keyup", onKeyUp.bind(this));

function deltaTime(){
	return app.stage.ticker.elapsedMS/1000;
}
function updateAll(){
	var dTime = deltaTime();
	move(dTime);
	//console.log("X: "+ moveX + "Y: "+ moveY);

}
app.stage.ticker.add(updateAll);