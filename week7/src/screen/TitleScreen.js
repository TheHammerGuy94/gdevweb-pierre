import { Container,utils, Text,Graphics,Sprite} from "pixi.js";
var Config = require('Config');

class TitleScreen extends Container{
    constructor(){
        super();
        drawGrid(this);
        this.interactive = true;

        var cache = utils.TextureCache;
        var image = new Sprite(cache["assets/images/Circle.png"])
        image.width = image.height = 250;
        this.addChild(image);

        var maskGraphics = new Graphics();
        maskGraphics.beginFill("0x000000");
        maskGraphics.drawCircle(50,50,225);
        maskGraphics.endFill();

        image.mask = maskGraphics;
        this.addChild(maskGraphics);

       
    }

    mousedown(e){
    	console.log("down");

    }
    mouseup(e){
    	console.log("up");

    }

    mousemove(e){
    	console.log(e.data.global.x+","+e.data.global.y);

    }
}

export default TitleScreen;
function drawGrid(stage){
	var grd = new PIXI.Graphics();
	grd.lineStyle(2,0x808080);
	for(let i = 100; i< Config.BUILD.HEIGHT; i+= 100){
		//draw H lines
		
		grd.moveTo(0,i);
		grd.lineTo(Config.BUILD.WIDTH,i);
	}
	grd.lineStyle(1,0x808080);
	for(let i = 10; i< Config.BUILD.HEIGHT; i+= 10){
		//draw smol H lines
		if(i%100 != 0){
			grd.moveTo(0,i);
			grd.lineTo(Config.BUILD.WIDTH,i);
		}
	}

	grd.lineStyle(2,0x808080);
	for(let i = 100; i< Config.BUILD.WIDTH; i+= 100){
		//draw V lines
		grd.moveTo(i,0);
		grd.lineTo(i,Config.BUILD.HEIGHT);
	}
	grd.lineStyle(1,0x808080);
	for(let i = 10; i< Config.BUILD.WIDTH; i+= 10){
		//draw V lines
		if(i%100 != 0){
			grd.moveTo(i,0);
			grd.lineTo(i,Config.BUILD.HEIGHT);
		}
	}
	stage.addChild(grd);
}

