/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/images/Circle.png\", \"assets/images/Square.png\", \"assets/images/Triangle.png\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9pbWFnZXMvQ2lyY2xlLnBuZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9TcXVhcmUucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL1RyaWFuZ2xlLnBuZ1wiXG5cdF0sXG5cdFwiYXVkaW9cIjogW11cbn07Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFLQTtBQU5BIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiBDb25maWcuQlVJTEQuV0lEVEgsXHJcbiAgICBoZWlnaHQ6IENvbmZpZy5CVUlMRC5IRUlHSFRcclxufSlcclxuXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSB7XHJcbiAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSAnbG9hZGluZycpIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBNYWluLnN0YXJ0KCk7XHJcbn0pO1xyXG5cclxuZXhwb3J0IHtwaXhpYXBwIGFzIEFwcH07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar TitleScreen = function (_Container) {\n\t_inherits(TitleScreen, _Container);\n\n\tfunction TitleScreen() {\n\t\t_classCallCheck(this, TitleScreen);\n\n\t\tvar _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n\t\tdrawGrid(_this);\n\t\t_this.interactive = true;\n\n\t\tvar cache = _pixi.utils.TextureCache;\n\t\tvar image = new _pixi.Sprite(cache[\"assets/images/Circle.png\"]);\n\t\timage.width = image.height = 250;\n\t\t_this.addChild(image);\n\n\t\tvar maskGraphics = new _pixi.Graphics();\n\t\tmaskGraphics.beginFill(\"0x000000\");\n\t\tmaskGraphics.drawCircle(50, 50, 225);\n\t\tmaskGraphics.endFill();\n\n\t\timage.mask = maskGraphics;\n\t\t_this.addChild(maskGraphics);\n\n\t\treturn _this;\n\t}\n\n\t_createClass(TitleScreen, [{\n\t\tkey: \"mousedown\",\n\t\tvalue: function mousedown(e) {\n\t\t\tconsole.log(\"down\");\n\t\t}\n\t}, {\n\t\tkey: \"mouseup\",\n\t\tvalue: function mouseup(e) {\n\t\t\tconsole.log(\"up\");\n\t\t}\n\t}, {\n\t\tkey: \"mousemove\",\n\t\tvalue: function mousemove(e) {\n\t\t\tconsole.log(e.data.global.x + \",\" + e.data.global.y);\n\t\t}\n\t}]);\n\n\treturn TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;\n\nfunction drawGrid(stage) {\n\tvar grd = new PIXI.Graphics();\n\tgrd.lineStyle(2, 0x808080);\n\tfor (var i = 100; i < Config.BUILD.HEIGHT; i += 100) {\n\t\t//draw H lines\n\n\t\tgrd.moveTo(0, i);\n\t\tgrd.lineTo(Config.BUILD.WIDTH, i);\n\t}\n\tgrd.lineStyle(1, 0x808080);\n\tfor (var _i = 10; _i < Config.BUILD.HEIGHT; _i += 10) {\n\t\t//draw smol H lines\n\t\tif (_i % 100 != 0) {\n\t\t\tgrd.moveTo(0, _i);\n\t\t\tgrd.lineTo(Config.BUILD.WIDTH, _i);\n\t\t}\n\t}\n\n\tgrd.lineStyle(2, 0x808080);\n\tfor (var _i2 = 100; _i2 < Config.BUILD.WIDTH; _i2 += 100) {\n\t\t//draw V lines\n\t\tgrd.moveTo(_i2, 0);\n\t\tgrd.lineTo(_i2, Config.BUILD.HEIGHT);\n\t}\n\tgrd.lineStyle(1, 0x808080);\n\tfor (var _i3 = 10; _i3 < Config.BUILD.WIDTH; _i3 += 10) {\n\t\t//draw V lines\n\t\tif (_i3 % 100 != 0) {\n\t\t\tgrd.moveTo(_i3, 0);\n\t\t\tgrd.lineTo(_i3, Config.BUILD.HEIGHT);\n\t\t}\n\t}\n\tstage.addChild(grd);\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsdXRpbHMsIFRleHQsR3JhcGhpY3MsU3ByaXRlfSBmcm9tIFwicGl4aS5qc1wiO1xyXG52YXIgQ29uZmlnID0gcmVxdWlyZSgnQ29uZmlnJyk7XHJcblxyXG5jbGFzcyBUaXRsZVNjcmVlbiBleHRlbmRzIENvbnRhaW5lcntcclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBkcmF3R3JpZCh0aGlzKTtcclxuICAgICAgICB0aGlzLmludGVyYWN0aXZlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdmFyIGNhY2hlID0gdXRpbHMuVGV4dHVyZUNhY2hlO1xyXG4gICAgICAgIHZhciBpbWFnZSA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL0NpcmNsZS5wbmdcIl0pXHJcbiAgICAgICAgaW1hZ2Uud2lkdGggPSBpbWFnZS5oZWlnaHQgPSAyNTA7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChpbWFnZSk7XHJcblxyXG4gICAgICAgIHZhciBtYXNrR3JhcGhpY3MgPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICBtYXNrR3JhcGhpY3MuYmVnaW5GaWxsKFwiMHgwMDAwMDBcIik7XHJcbiAgICAgICAgbWFza0dyYXBoaWNzLmRyYXdDaXJjbGUoNTAsNTAsMjI1KTtcclxuICAgICAgICBtYXNrR3JhcGhpY3MuZW5kRmlsbCgpO1xyXG5cclxuICAgICAgICBpbWFnZS5tYXNrID0gbWFza0dyYXBoaWNzO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQobWFza0dyYXBoaWNzKTtcclxuXHJcbiAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBtb3VzZWRvd24oZSl7XHJcbiAgICBcdGNvbnNvbGUubG9nKFwiZG93blwiKTtcclxuXHJcbiAgICB9XHJcbiAgICBtb3VzZXVwKGUpe1xyXG4gICAgXHRjb25zb2xlLmxvZyhcInVwXCIpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBtb3VzZW1vdmUoZSl7XHJcbiAgICBcdGNvbnNvbGUubG9nKGUuZGF0YS5nbG9iYWwueCtcIixcIitlLmRhdGEuZ2xvYmFsLnkpO1xyXG5cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVGl0bGVTY3JlZW47XHJcbmZ1bmN0aW9uIGRyYXdHcmlkKHN0YWdlKXtcclxuXHR2YXIgZ3JkID0gbmV3IFBJWEkuR3JhcGhpY3MoKTtcclxuXHRncmQubGluZVN0eWxlKDIsMHg4MDgwODApO1xyXG5cdGZvcihsZXQgaSA9IDEwMDsgaTwgQ29uZmlnLkJVSUxELkhFSUdIVDsgaSs9IDEwMCl7XHJcblx0XHQvL2RyYXcgSCBsaW5lc1xyXG5cdFx0XHJcblx0XHRncmQubW92ZVRvKDAsaSk7XHJcblx0XHRncmQubGluZVRvKENvbmZpZy5CVUlMRC5XSURUSCxpKTtcclxuXHR9XHJcblx0Z3JkLmxpbmVTdHlsZSgxLDB4ODA4MDgwKTtcclxuXHRmb3IobGV0IGkgPSAxMDsgaTwgQ29uZmlnLkJVSUxELkhFSUdIVDsgaSs9IDEwKXtcclxuXHRcdC8vZHJhdyBzbW9sIEggbGluZXNcclxuXHRcdGlmKGklMTAwICE9IDApe1xyXG5cdFx0XHRncmQubW92ZVRvKDAsaSk7XHJcblx0XHRcdGdyZC5saW5lVG8oQ29uZmlnLkJVSUxELldJRFRILGkpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Z3JkLmxpbmVTdHlsZSgyLDB4ODA4MDgwKTtcclxuXHRmb3IobGV0IGkgPSAxMDA7IGk8IENvbmZpZy5CVUlMRC5XSURUSDsgaSs9IDEwMCl7XHJcblx0XHQvL2RyYXcgViBsaW5lc1xyXG5cdFx0Z3JkLm1vdmVUbyhpLDApO1xyXG5cdFx0Z3JkLmxpbmVUbyhpLENvbmZpZy5CVUlMRC5IRUlHSFQpO1xyXG5cdH1cclxuXHRncmQubGluZVN0eWxlKDEsMHg4MDgwODApO1xyXG5cdGZvcihsZXQgaSA9IDEwOyBpPCBDb25maWcuQlVJTEQuV0lEVEg7IGkrPSAxMCl7XHJcblx0XHQvL2RyYXcgViBsaW5lc1xyXG5cdFx0aWYoaSUxMDAgIT0gMCl7XHJcblx0XHRcdGdyZC5tb3ZlVG8oaSwwKTtcclxuXHRcdFx0Z3JkLmxpbmVUbyhpLENvbmZpZy5CVUlMRC5IRUlHSFQpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRzdGFnZS5hZGRDaGlsZChncmQpO1xyXG59XHJcblxyXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTs7Ozs7OztBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBbUJBO0FBQ0E7OztBQUNBO0FBQ0E7QUFFQTs7O0FBQ0E7QUFDQTtBQUVBOzs7QUFFQTtBQUNBO0FBRUE7Ozs7QUFsQ0E7QUFDQTtBQW9DQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });